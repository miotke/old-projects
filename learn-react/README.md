# learn-React<br>
[![Review](https://img.shields.io/badge/Project-InProgress-orange.svg)]()<br>
Just a place that I'm learning React.<br>
If I find a tutorial through my learnings they will be included inside of each directory.<br><br><br>

<b>PWA-Experiment<br>[![Review](https://img.shields.io/badge/Project-Complete-brightgreen.svg)]()</b><br>
https://engineering.musefind.com/build-your-first-progressive-web-app-with-react-8e1449c575cd <br><br>

<b>React Start Kit<br>[![Review](https://img.shields.io/badge/Project-Complete-brightgreen.svg)]()</b><br>
http://andrewhfarmer.com/build-your-own-starter/#0-intro<br><br>

<b>React Native Calculator<br>[![Review](https://img.shields.io/badge/Project-Complete-brightgreen.svg)]()</b><br>
Part 1: https://kylewbanks.com/blog/react-native-tutorial-part-1-hello-react<br>
Part 2: https://kylewbanks.com/blog/react-native-tutorial-part-2-designing-a-calculator<br>
Part 3: https://kylewbanks.com/blog/react-native-tutorial-part-3-developing-a-calculator<br><br>

<b>Building Applications with React and Flux - Cory House<br>[![Review](https://img.shields.io/badge/Project-InProgress-orange.svg)]()</b><br>
https://app.pluralsight.com/library/courses/react-flux-building-applications/table-of-contents<br><br>

<b>Todo List<br>[![Review](https://img.shields.io/badge/Project-InProgress-orange.svg)]()</b><br>
https://scotch.io/tutorials/create-a-simple-to-do-app-with-react<br>
https://www.kirupa.com/react/simple_todo_app_react.htm<br><br>

<b>REACT JS TUTORIAL - LearnCode.academy<br>
[![Review](https://img.shields.io/badge/Project-InProgress-orange.svg)]()</b><br>
https://www.youtube.com/watch?v=MhkGQAoc7bc
