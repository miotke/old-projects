# boiler
[![Review](https://img.shields.io/badge/Project-Complete-brightgreen.svg)]()<br>
Created a boilerplate to use on all projects which includes post-css and gulp scripts.<br>

#How To Use<br>
1) Run "npm install" in terminal after downloading this boilerplate to install the most recent npm dependencies to your project.<br>
2) Open your command prompt and navigate to your working directory.
<br>
3) Type 'gulp watch' without quotes. A browser will open with a localhost:3000 address.



#Packages Included
<b>Dependencies</b><br>
  -normalize.css<br><br>
<b>DevDependencies:</b><br>
  -autoprefixer<br>
  -browser-sync<br>
  -gulp<br>
  -gulp-postcss<br>
  -gulp-watch<br>
  -postcss-import<br>
  -postcss-mixins<br>
  -postcss-nested<br>
  -postcss-simple-vars

